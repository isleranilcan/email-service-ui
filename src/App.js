import Header from './components/Header'
import AddEmail from './components/AddEmail'
import Notifications, { notify } from 'react-notify-toast';

function App() {
  return (
    <div className="container">
      <Notifications />
      <Header />
      <AddEmail />
    </div>
  );
}

export default App;
