import { useState } from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Spinner from 'react-bootstrap/Spinner'
import { notify } from 'react-notify-toast';

const AddEmail = () => {

    const [emailAddress, setEmailAddress] = useState('')
    const [subject, setSubject] = useState('')
    const [message, setMessage] = useState('')
    const [loading, toggleLoading] = useState(false)


    const onSaveButtonClicked = () => {
        toggleLoading(loading => !loading);

        if (!emailAddress) {
            alert('Please add a recipient address.')
            toggleLoading(loading => !loading);
            return
        }

        saveEmail({ emailAddress, subject, message })
    }

    const saveEmail = async (email) => {

        const requestoptions = {
            method: 'POST',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify(email)
        };

        fetch('http://localhost:8080/api/v1/email', requestoptions)
            .then(async res => {
                if (!res.ok) {
                    return Promise.reject(res.status);
                }
                notify.show('Message is saved successfully!', 'success', 2500);
                emptyFields();
                toggleLoading(loading => !loading);
            })
            .catch(error => {
                notify.show('Message is not saved!', 'error', 2500);
                console.error('Message is not saved!', error);
                toggleLoading(loading => !loading);
            })
    }

    const emptyFields = () => {
        setEmailAddress('')
        setSubject('')
        setMessage('')
    }

    return (
        <Form>
            <Form.Group>
                <Form.Label>To</Form.Label>
                <Form.Control type="email" placeholder="name@example.com" disabled={loading}
                    value={emailAddress} onChange={(e) => setEmailAddress(e.target.value)} />
            </Form.Group>
            <Form.Group>
                <Form.Label>Subject</Form.Label>
                <Form.Control type="text" placeholder="Subject of email" disabled={loading}
                    value={subject} onChange={(e) => setSubject(e.target.value)} />
            </Form.Group>
            <Form.Group>
                <Form.Label>Message</Form.Label>
                <Form.Control as="textarea" placeholder="Message content" rows={3} disabled={loading}
                    value={message} onChange={(e) => setMessage(e.target.value)} />
            </Form.Group>

            <Button onClick={onSaveButtonClicked} disabled={loading}>
                {loading && <Spinner
                    as="span"
                    animation="border"
                    size="sm"
                    role="status"
                    aria-hidden="true"
                />}
                {loading && <span> Saving...</span>}
                {!loading && <span>Save message</span>}
            </Button>{' '}
        </Form>
    )
}



export default AddEmail
